package main

import (
	"flag"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	register "gitlab.com/dovgopoly/register-contract/internal/generated"
	"log"
	"math/big"
	"strings"
)

var url = flag.String("url", "http://127.0.0.1:7545", "url")
var contractAddress = flag.String("contract-address", "", "contract address")
var ownerPrivateKey = flag.String("owner-pk", "", "owner private key")

func main() {
	flag.Parse()

	if len(*contractAddress) == 0 {
		log.Fatal("contract address should be specified")
	}
	if len(*ownerPrivateKey) == 0 {
		log.Fatal("contract owner's private key should be specified")
	}

	ownerPrivateKeyECDSA, err := crypto.HexToECDSA(*ownerPrivateKey)
	if err != nil {
		log.Fatal("invalid private key provided")
	}

	client, err := ethclient.Dial(*url)
	if err != nil {
		log.Fatalf("failed to connect: %v", err)
	}

	r, err := register.NewRegister(common.HexToAddress(*contractAddress), client)
	if err != nil {
		log.Fatalf("failed to get an instance of the smart contract: %v", err)
	}

	ownerAddress, err := r.Owner(nil)
	if err != nil {
		log.Fatalf("failed to get owner address: %v", err)
	}
	log.Printf("owner's address is: %s\n", ownerAddress.String())

	addTransaction := func(tx register.RegisterBankTransaction) error {
		opts, err := bind.NewKeyedTransactorWithChainID(ownerPrivateKeyECDSA, big.NewInt(1337))
		if err != nil {
			return err
		}
		_, err = r.RegisterTransactor.AddTransaction(
			opts,
			tx.TransactionID,
			tx.SenderID,
			tx.ReceiverID,
			tx.Amount,
			tx.Date,
			tx.Note,
		)
		return err
	}

	tx := register.RegisterBankTransaction{
		TransactionID: "TRANSACTION_0",
		SenderID:      "CLIENT_0",
		ReceiverID:    "CLIENT_1",
		Amount:        big.NewInt(0),
		Date:          big.NewInt(0),
	}
	if err = addTransaction(tx); err != nil {
		if !strings.Contains(err.Error(), "Transaction with such id already exists") {
			log.Fatalf("failed to add transaction: %v", err)
		}
	}

	txFromStorage, err := r.GetTransactionByID(nil, tx.TransactionID)
	if err != nil {
		log.Fatalf("failed to get transaction by id: %v", err.Error())
	}

	log.Printf("%v", txFromStorage)
}
