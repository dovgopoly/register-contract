const Register = artifacts.require("Register");

contract("Register", function (accounts) {
    const clients = ["CLIENT_0", "CLIENT_1", "CLIENT_2"]

    const txs = [
        {
            transactionID: "TX_0",
            senderID: clients[0],
            receiverID: clients[1],
            amount: 0,
            date: 0,
            note: "Empty"
        },
        {
            transactionID: "TX_1",
            senderID: clients[0],
            receiverID: clients[2],
            amount: 0,
            date: 0,
            note: "Empty"
        },
        {
            transactionID: "TX_2",
            senderID: clients[1],
            receiverID: clients[0],
            amount: 0,
            date: 0,
            note: "Empty"
        },
        {
            transactionID: "TX_3",
            senderID: clients[1],
            receiverID: clients[2],
            amount: 0,
            date: 0,
            note: "Empty"
        },
        {
            transactionID: "TX_4",
            senderID: clients[2],
            receiverID: clients[0],
            amount: 0,
            date: 0,
            note: "Empty"
        },
        {
            transactionID: "TX_5",
            senderID: clients[2],
            receiverID: clients[1],
            amount: 0,
            date: 0,
            note: "Empty"
        },
    ]

    const [owner, nonOwner] = accounts

    let register = null

    const addTransaction =
        (sender, tx) => register.addTransaction(
            tx.transactionID,
            tx.senderID,
            tx.receiverID,
            tx.amount,
            tx.date,
            tx.note,
            {from: sender}
        )

    beforeEach(async () => {
        register = await Register.new()
    })

    it("should add transaction from owner account", async () => {
        try {
            await addTransaction(owner, txs[0])
        } catch (e) {
            assert(false, `caught unexpected error: ${e.message}`)
        }
    })

    it("should not add transaction from non-owner account", async () => {
        try {
            await addTransaction(nonOwner, txs[0])
        } catch (e) {
            assert(
                e.message.includes("Ownable: caller is not the owner"),
                `caught unexpected error: ${e.message}`
            )
            return
        }
        assert(false, "transaction expected to be reverted")
    })

    it("should not add a few transactions with the same id", async () => {
        try {
            await addTransaction(owner, txs[0])
            await addTransaction(owner, txs[0])
        } catch (e) {
            assert(
                e.message.includes("Transaction with such id already exists"),
                `caught unexpected error: ${e.message}`
            )
            return
        }
        assert(false, "transaction expected to be reverted")
    })

    it("should not add a transaction where senderID is equal receiverID", async () => {
        let wrongTx = {
            transactionID: "WRONG_TX",
            senderID: clients[0],
            receiverID: clients[0],
            amount: 0,
            date: 0,
            note: "Empty",
        }
        try {
            await addTransaction(owner, wrongTx)
        } catch (e) {
            assert(
                e.message.includes("Sender's id should not be equal receiver's one"),
                `caught unexpected error: ${e.message}`
            )
            return
        }
        assert(false, "transaction expected to be reverted")
    })

    it("should get transaction by id properly", async () => {
        for (const tx of txs) {
            await addTransaction(owner, tx)
        }
        for (const tx of txs) {
            const txOwner =
                await register.getTransactionByID(tx.transactionID, {from: owner})
            const txNonOwner =
                await register.getTransactionByID(tx.transactionID, {from: nonOwner})
            assert.equal(txOwner.transactionID, tx.transactionID, "wrong transaction id")
            assert.equal(
                JSON.stringify(txOwner),
                JSON.stringify(txNonOwner),
                "transactions expected to be equal"
            )
        }
    })

    it("should not find transaction when id does not exist", async () => {
        try {
            await register.getTransactionByID(txs[0].transactionID)
        } catch (e) {
            assert(
                e.message.includes("Transaction with such id has not been found"),
                `caught unexpected error: ${e.message}`
            )
            return
        }
        assert(false, "transaction expected to be reverted")
    })

    it("should get transaction list by client id properly", async () => {
        for (const tx of txs) {
            await addTransaction(owner, tx)
        }

        const id = clients[0]

        const response = await register.getAllTransactionsByClientID(id)

        const txAsSender = response.asSender.map(tx => tx.transactionID).sort()
        const txAsReceiver = response.asReceiver.map(tx => tx.transactionID).sort()

        assert.equal(
            JSON.stringify(txAsSender),
            JSON.stringify([txs[0].transactionID, txs[1].transactionID].sort()),
            "wrong sender transaction ids"
        )
        assert.equal(
            JSON.stringify(txAsReceiver),
            JSON.stringify([txs[2].transactionID, txs[4].transactionID].sort()),
            "wrong receiver transaction ids"
        )
    })

    it("hashes are different", async () => {
        await addTransaction(owner, txs[0])
        await addTransaction(owner, txs[1])
        const lhs = await register.getTransactionByID(txs[0].transactionID)
        const rhs = await register.getTransactionByID(txs[1].transactionID)
        assert.notEqual(lhs.hash, rhs.hash, "hashes should not be equal")
    })

    it("should change balance properly", async() => {
        const balance = await web3.eth.getBalance(owner)

        await addTransaction(owner, txs[0])
        const afterAdding = await web3.eth.getBalance(owner)
        assert(afterAdding < balance, "balance should be less after adding transaction")

        await register.getTransactionByID(txs[0].transactionID, {from: owner})
        const afterGettingByID = await web3.eth.getBalance(owner)
        assert.equal(
            afterAdding,
            afterGettingByID,
            "balance should not change after calling view function"
        )

        const id = clients[0]
        await register.getAllTransactionsByClientID(id, {from: owner})
        const afterGettingClientTxs = await web3.eth.getBalance(owner)
        assert.equal(
            afterAdding,
            afterGettingClientTxs,
            "balance should not change after calling view function"
        )
    })
})
