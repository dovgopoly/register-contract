## Tests

```
npm test
```

## Go
```
# setup ganache
truffle migrate --network ganache
go run ./cmd/main/main.go -url=http://127.0.0.1:7545 -contract-address=0x7b28BB52Ea6254C0a1e7C4c67999Df61E2d832Cc -owner-pk=bbd7b98a847e963f04df7931386271028beb3b7e6c90edecbaa07091ff3c1ad1
```
