// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package register

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// RegisterBankTransaction is an auto generated low-level Go binding around an user-defined struct.
type RegisterBankTransaction struct {
	TransactionID string
	SenderID      string
	ReceiverID    string
	Amount        *big.Int
	Date          *big.Int
	Note          string
	Hash          [32]byte
}

// RegisterMetaData contains all meta data concerning the Register contract.
var RegisterMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"transactionID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"senderID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"receiverID\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"date\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"note\",\"type\":\"string\"}],\"name\":\"addTransaction\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"id\",\"type\":\"string\"}],\"name\":\"getAllTransactionsByClientID\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"transactionID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"senderID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"receiverID\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"date\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"note\",\"type\":\"string\"},{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"}],\"internalType\":\"structRegister.BankTransaction[]\",\"name\":\"asSender\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"transactionID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"senderID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"receiverID\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"date\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"note\",\"type\":\"string\"},{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"}],\"internalType\":\"structRegister.BankTransaction[]\",\"name\":\"asReceiver\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"id\",\"type\":\"string\"}],\"name\":\"getTransactionByID\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"transactionID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"senderID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"receiverID\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"date\",\"type\":\"uint256\"},{\"internalType\":\"string\",\"name\":\"note\",\"type\":\"string\"},{\"internalType\":\"bytes32\",\"name\":\"hash\",\"type\":\"bytes32\"}],\"internalType\":\"structRegister.BankTransaction\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b5061002d61002261003260201b60201c565b61003a60201b60201c565b6100fe565b600033905090565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050816000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508173ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a35050565b612375806200010e6000396000f3fe608060405234801561001057600080fd5b50600436106100625760003560e01c80630691e1ee1461006757806370a9db3114610098578063715018a6146100c85780638da5cb5b146100d2578063f2fde38b146100f0578063f5f3fb001461010c575b600080fd5b610081600480360381019061007c91906117c8565b610128565b60405161008f929190611a3e565b60405180910390f35b6100b260048036038101906100ad91906117c8565b61015c565b6040516100bf9190611b26565b60405180910390f35b6100d0610593565b005b6100da61061b565b6040516100e79190611b89565b60405180910390f35b61010a60048036038101906101059190611bd0565b610644565b005b61012660048036038101906101219190611c29565b61073b565b005b60608061013f8360016109b390919063ffffffff16565b9150610155836001610dd290919063ffffffff16565b9050915091565b61016461158b565b6002826040516101749190611d62565b908152602001604051809103902060009054906101000a900460ff166101cf576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016101c690611dfc565b60405180910390fd5b60005b6001805490508110156105525761029f83600183815481106101f7576101f6611e1c565b5b9060005260206000209060070201600001805461021390611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461023f90611e7a565b801561028c5780601f106102615761010080835404028352916020019161028c565b820191906000526020600020905b81548152906001019060200180831161026f57829003601f168201915b50505050506111f190919063ffffffff16565b1561053f57600181815481106102b8576102b7611e1c565b5b90600052602060002090600702016040518060e00160405290816000820180546102e190611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461030d90611e7a565b801561035a5780601f1061032f5761010080835404028352916020019161035a565b820191906000526020600020905b81548152906001019060200180831161033d57829003601f168201915b5050505050815260200160018201805461037390611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461039f90611e7a565b80156103ec5780601f106103c1576101008083540402835291602001916103ec565b820191906000526020600020905b8154815290600101906020018083116103cf57829003601f168201915b5050505050815260200160028201805461040590611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461043190611e7a565b801561047e5780601f106104535761010080835404028352916020019161047e565b820191906000526020600020905b81548152906001019060200180831161046157829003601f168201915b5050505050815260200160038201548152602001600482015481526020016005820180546104ab90611e7a565b80601f01602080910402602001604051908101604052809291908181526020018280546104d790611e7a565b80156105245780601f106104f957610100808354040283529160200191610524565b820191906000526020600020905b81548152906001019060200180831161050757829003601f168201915b5050505050815260200160068201548152505091505061058e565b808061054a90611eda565b9150506101d2565b506040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161058590611f6e565b60405180910390fd5b919050565b61059b61120c565b73ffffffffffffffffffffffffffffffffffffffff166105b961061b565b73ffffffffffffffffffffffffffffffffffffffff161461060f576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161060690611fda565b60405180910390fd5b6106196000611214565b565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b61064c61120c565b73ffffffffffffffffffffffffffffffffffffffff1661066a61061b565b73ffffffffffffffffffffffffffffffffffffffff16146106c0576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016106b790611fda565b60405180910390fd5b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff160361072f576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016107269061206c565b60405180910390fd5b61073881611214565b50565b61074361120c565b73ffffffffffffffffffffffffffffffffffffffff1661076161061b565b73ffffffffffffffffffffffffffffffffffffffff16146107b7576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016107ae90611fda565b60405180910390fd5b6002866040516107c79190611d62565b908152602001604051809103902060009054906101000a900460ff1615610823576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161081a906120fe565b60405180910390fd5b61083684866111f190919063ffffffff16565b15610876576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161086d90612190565b60405180910390fd5b60016002876040516108889190611d62565b908152602001604051809103902060006101000a81548160ff02191690831515021790555060016040518060e001604052808881526020018781526020018681526020018581526020018481526020018381526020016108eb89898989896112d8565b815250908060018154018082558091505060019003906000526020600020906007020160009091909190915060008201518160000190805190602001906109339291906115cb565b5060208201518160010190805190602001906109509291906115cb565b50604082015181600201908051906020019061096d9291906115cb565b50606082015181600301556080820151816004015560a082015181600501908051906020019061099e9291906115cb565b5060c082015181600601555050505050505050565b606060006109c18484611324565b90508067ffffffffffffffff8111156109dd576109dc61169d565b5b604051908082528060200260200182016040528015610a1657816020015b610a0361158b565b8152602001906001900390816109fb5790505b50915060005b8480549050811015610dca57610ae784868381548110610a3f57610a3e611e1c565b5b90600052602060002090600702016001018054610a5b90611e7a565b80601f0160208091040260200160405190810160405280929190818152602001828054610a8790611e7a565b8015610ad45780601f10610aa957610100808354040283529160200191610ad4565b820191906000526020600020905b815481529060010190602001808311610ab757829003601f168201915b50505050506111f190919063ffffffff16565b15610db757848181548110610aff57610afe611e1c565b5b90600052602060002090600702016040518060e0016040529081600082018054610b2890611e7a565b80601f0160208091040260200160405190810160405280929190818152602001828054610b5490611e7a565b8015610ba15780601f10610b7657610100808354040283529160200191610ba1565b820191906000526020600020905b815481529060010190602001808311610b8457829003601f168201915b50505050508152602001600182018054610bba90611e7a565b80601f0160208091040260200160405190810160405280929190818152602001828054610be690611e7a565b8015610c335780601f10610c0857610100808354040283529160200191610c33565b820191906000526020600020905b815481529060010190602001808311610c1657829003601f168201915b50505050508152602001600282018054610c4c90611e7a565b80601f0160208091040260200160405190810160405280929190818152602001828054610c7890611e7a565b8015610cc55780601f10610c9a57610100808354040283529160200191610cc5565b820191906000526020600020905b815481529060010190602001808311610ca857829003601f168201915b505050505081526020016003820154815260200160048201548152602001600582018054610cf290611e7a565b80601f0160208091040260200160405190810160405280929190818152602001828054610d1e90611e7a565b8015610d6b5780601f10610d4057610100808354040283529160200191610d6b565b820191906000526020600020905b815481529060010190602001808311610d4e57829003601f168201915b5050505050815260200160068201548152505083600184610d8c91906121b0565b81518110610d9d57610d9c611e1c565b5b60200260200101819052508180610db3906121e4565b9250505b8080610dc290611eda565b915050610a1c565b505092915050565b60606000610de08484611324565b90508067ffffffffffffffff811115610dfc57610dfb61169d565b5b604051908082528060200260200182016040528015610e3557816020015b610e2261158b565b815260200190600190039081610e1a5790505b50915060005b84805490508110156111e957610f0684868381548110610e5e57610e5d611e1c565b5b90600052602060002090600702016002018054610e7a90611e7a565b80601f0160208091040260200160405190810160405280929190818152602001828054610ea690611e7a565b8015610ef35780601f10610ec857610100808354040283529160200191610ef3565b820191906000526020600020905b815481529060010190602001808311610ed657829003601f168201915b50505050506111f190919063ffffffff16565b156111d657848181548110610f1e57610f1d611e1c565b5b90600052602060002090600702016040518060e0016040529081600082018054610f4790611e7a565b80601f0160208091040260200160405190810160405280929190818152602001828054610f7390611e7a565b8015610fc05780601f10610f9557610100808354040283529160200191610fc0565b820191906000526020600020905b815481529060010190602001808311610fa357829003601f168201915b50505050508152602001600182018054610fd990611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461100590611e7a565b80156110525780601f1061102757610100808354040283529160200191611052565b820191906000526020600020905b81548152906001019060200180831161103557829003601f168201915b5050505050815260200160028201805461106b90611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461109790611e7a565b80156110e45780601f106110b9576101008083540402835291602001916110e4565b820191906000526020600020905b8154815290600101906020018083116110c757829003601f168201915b50505050508152602001600382015481526020016004820154815260200160058201805461111190611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461113d90611e7a565b801561118a5780601f1061115f5761010080835404028352916020019161118a565b820191906000526020600020905b81548152906001019060200180831161116d57829003601f168201915b50505050508152602001600682015481525050836001846111ab91906121b0565b815181106111bc576111bb611e1c565b5b602002602001018190525081806111d2906121e4565b9250505b80806111e190611eda565b915050610e3b565b505092915050565b60008180519060200120838051906020012014905092915050565b600033905090565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050816000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508173ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a35050565b60008585856112e68661142b565b6112ef8661142b565b60405160200161130395949392919061220d565b60405160208183030381529060405280519060200120905095945050505050565b6000806000905060005b8480549050811015611420576113f98486838154811061135157611350611e1c565b5b9060005260206000209060070201600101805461136d90611e7a565b80601f016020809104026020016040519081016040528092919081815260200182805461139990611e7a565b80156113e65780601f106113bb576101008083540402835291602001916113e6565b820191906000526020600020905b8154815290600101906020018083116113c957829003601f168201915b50505050506111f190919063ffffffff16565b1561140d57818061140990611eda565b9250505b808061141890611eda565b91505061132e565b508091505092915050565b606060008203611472576040518060400160405280600181526020017f30000000000000000000000000000000000000000000000000000000000000008152509050611586565b600082905060005b600082146114a457808061148d90611eda565b915050600a8261149d9190612287565b915061147a565b60008167ffffffffffffffff8111156114c0576114bf61169d565b5b6040519080825280601f01601f1916602001820160405280156114f25781602001600182028036833780820191505090505b5090505b6000851461157f5760018261150b91906121b0565b9150600a8561151a91906122b8565b603061152691906122e9565b60f81b81838151811061153c5761153b611e1c565b5b60200101907effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff1916908160001a905350600a856115789190612287565b94506114f6565b8093505050505b919050565b6040518060e00160405280606081526020016060815260200160608152602001600081526020016000815260200160608152602001600080191681525090565b8280546115d790611e7a565b90600052602060002090601f0160209004810192826115f95760008555611640565b82601f1061161257805160ff1916838001178555611640565b82800160010185558215611640579182015b8281111561163f578251825591602001919060010190611624565b5b50905061164d9190611651565b5090565b5b8082111561166a576000816000905550600101611652565b5090565b6000604051905090565b600080fd5b600080fd5b600080fd5b600080fd5b6000601f19601f8301169050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052604160045260246000fd5b6116d58261168c565b810181811067ffffffffffffffff821117156116f4576116f361169d565b5b80604052505050565b600061170761166e565b905061171382826116cc565b919050565b600067ffffffffffffffff8211156117335761173261169d565b5b61173c8261168c565b9050602081019050919050565b82818337600083830152505050565b600061176b61176684611718565b6116fd565b90508281526020810184848401111561178757611786611687565b5b611792848285611749565b509392505050565b600082601f8301126117af576117ae611682565b5b81356117bf848260208601611758565b91505092915050565b6000602082840312156117de576117dd611678565b5b600082013567ffffffffffffffff8111156117fc576117fb61167d565b5b6118088482850161179a565b91505092915050565b600081519050919050565b600082825260208201905092915050565b6000819050602082019050919050565b600081519050919050565b600082825260208201905092915050565b60005b8381101561187757808201518184015260208101905061185c565b83811115611886576000848401525b50505050565b60006118978261183d565b6118a18185611848565b93506118b1818560208601611859565b6118ba8161168c565b840191505092915050565b6000819050919050565b6118d8816118c5565b82525050565b6000819050919050565b6118f1816118de565b82525050565b600060e0830160008301518482036000860152611914828261188c565b9150506020830151848203602086015261192e828261188c565b91505060408301518482036040860152611948828261188c565b915050606083015161195d60608601826118cf565b50608083015161197060808601826118cf565b5060a083015184820360a0860152611988828261188c565b91505060c083015161199d60c08601826118e8565b508091505092915050565b60006119b483836118f7565b905092915050565b6000602082019050919050565b60006119d482611811565b6119de818561181c565b9350836020820285016119f08561182d565b8060005b85811015611a2c5784840389528151611a0d85826119a8565b9450611a18836119bc565b925060208a019950506001810190506119f4565b50829750879550505050505092915050565b60006040820190508181036000830152611a5881856119c9565b90508181036020830152611a6c81846119c9565b90509392505050565b600060e0830160008301518482036000860152611a92828261188c565b91505060208301518482036020860152611aac828261188c565b91505060408301518482036040860152611ac6828261188c565b9150506060830151611adb60608601826118cf565b506080830151611aee60808601826118cf565b5060a083015184820360a0860152611b06828261188c565b91505060c0830151611b1b60c08601826118e8565b508091505092915050565b60006020820190508181036000830152611b408184611a75565b905092915050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000611b7382611b48565b9050919050565b611b8381611b68565b82525050565b6000602082019050611b9e6000830184611b7a565b92915050565b611bad81611b68565b8114611bb857600080fd5b50565b600081359050611bca81611ba4565b92915050565b600060208284031215611be657611be5611678565b5b6000611bf484828501611bbb565b91505092915050565b611c06816118c5565b8114611c1157600080fd5b50565b600081359050611c2381611bfd565b92915050565b60008060008060008060c08789031215611c4657611c45611678565b5b600087013567ffffffffffffffff811115611c6457611c6361167d565b5b611c7089828a0161179a565b965050602087013567ffffffffffffffff811115611c9157611c9061167d565b5b611c9d89828a0161179a565b955050604087013567ffffffffffffffff811115611cbe57611cbd61167d565b5b611cca89828a0161179a565b9450506060611cdb89828a01611c14565b9350506080611cec89828a01611c14565b92505060a087013567ffffffffffffffff811115611d0d57611d0c61167d565b5b611d1989828a0161179a565b9150509295509295509295565b600081905092915050565b6000611d3c8261183d565b611d468185611d26565b9350611d56818560208601611859565b80840191505092915050565b6000611d6e8284611d31565b915081905092915050565b600082825260208201905092915050565b7f5472616e73616374696f6e2077697468207375636820696420686173206e6f7460008201527f206265656e20666f756e64000000000000000000000000000000000000000000602082015250565b6000611de6602b83611d79565b9150611df182611d8a565b604082019050919050565b60006020820190508181036000830152611e1581611dd9565b9050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052603260045260246000fd5b7f4e487b7100000000000000000000000000000000000000000000000000000000600052602260045260246000fd5b60006002820490506001821680611e9257607f821691505b602082108103611ea557611ea4611e4b565b5b50919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601160045260246000fd5b6000611ee5826118c5565b91507fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8203611f1757611f16611eab565b5b600182019050919050565b7f496e7465726e616c206572726f72000000000000000000000000000000000000600082015250565b6000611f58600e83611d79565b9150611f6382611f22565b602082019050919050565b60006020820190508181036000830152611f8781611f4b565b9050919050565b7f4f776e61626c653a2063616c6c6572206973206e6f7420746865206f776e6572600082015250565b6000611fc4602083611d79565b9150611fcf82611f8e565b602082019050919050565b60006020820190508181036000830152611ff381611fb7565b9050919050565b7f4f776e61626c653a206e6577206f776e657220697320746865207a65726f206160008201527f6464726573730000000000000000000000000000000000000000000000000000602082015250565b6000612056602683611d79565b915061206182611ffa565b604082019050919050565b6000602082019050818103600083015261208581612049565b9050919050565b7f5472616e73616374696f6e2077697468207375636820696420616c726561647960008201527f2065786973747300000000000000000000000000000000000000000000000000602082015250565b60006120e8602783611d79565b91506120f38261208c565b604082019050919050565b60006020820190508181036000830152612117816120db565b9050919050565b7f53656e64657227732069642073686f756c64206e6f7420626520657175616c2060008201527f72656365697665722773206f6e65000000000000000000000000000000000000602082015250565b600061217a602e83611d79565b91506121858261211e565b604082019050919050565b600060208201905081810360008301526121a98161216d565b9050919050565b60006121bb826118c5565b91506121c6836118c5565b9250828210156121d9576121d8611eab565b5b828203905092915050565b60006121ef826118c5565b91506000820361220257612201611eab565b5b600182039050919050565b60006122198288611d31565b91506122258287611d31565b91506122318286611d31565b915061223d8285611d31565b91506122498284611d31565b91508190509695505050505050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601260045260246000fd5b6000612292826118c5565b915061229d836118c5565b9250826122ad576122ac612258565b5b828204905092915050565b60006122c3826118c5565b91506122ce836118c5565b9250826122de576122dd612258565b5b828206905092915050565b60006122f4826118c5565b91506122ff836118c5565b9250827fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0382111561233457612333611eab565b5b82820190509291505056fea2646970667358221220b0107af1bf132726154183d6f4f9c577b45b16c2d73e36682fd8faf14b4da5c264736f6c634300080d0033",
}

// RegisterABI is the input ABI used to generate the binding from.
// Deprecated: Use RegisterMetaData.ABI instead.
var RegisterABI = RegisterMetaData.ABI

// RegisterBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use RegisterMetaData.Bin instead.
var RegisterBin = RegisterMetaData.Bin

// DeployRegister deploys a new Ethereum contract, binding an instance of Register to it.
func DeployRegister(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Register, error) {
	parsed, err := RegisterMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(RegisterBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Register{RegisterCaller: RegisterCaller{contract: contract}, RegisterTransactor: RegisterTransactor{contract: contract}, RegisterFilterer: RegisterFilterer{contract: contract}}, nil
}

// Register is an auto generated Go binding around an Ethereum contract.
type Register struct {
	RegisterCaller     // Read-only binding to the contract
	RegisterTransactor // Write-only binding to the contract
	RegisterFilterer   // Log filterer for contract events
}

// RegisterCaller is an auto generated read-only Go binding around an Ethereum contract.
type RegisterCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RegisterTransactor is an auto generated write-only Go binding around an Ethereum contract.
type RegisterTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RegisterFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type RegisterFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RegisterSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type RegisterSession struct {
	Contract     *Register         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// RegisterCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type RegisterCallerSession struct {
	Contract *RegisterCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// RegisterTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type RegisterTransactorSession struct {
	Contract     *RegisterTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// RegisterRaw is an auto generated low-level Go binding around an Ethereum contract.
type RegisterRaw struct {
	Contract *Register // Generic contract binding to access the raw methods on
}

// RegisterCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type RegisterCallerRaw struct {
	Contract *RegisterCaller // Generic read-only contract binding to access the raw methods on
}

// RegisterTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type RegisterTransactorRaw struct {
	Contract *RegisterTransactor // Generic write-only contract binding to access the raw methods on
}

// NewRegister creates a new instance of Register, bound to a specific deployed contract.
func NewRegister(address common.Address, backend bind.ContractBackend) (*Register, error) {
	contract, err := bindRegister(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Register{RegisterCaller: RegisterCaller{contract: contract}, RegisterTransactor: RegisterTransactor{contract: contract}, RegisterFilterer: RegisterFilterer{contract: contract}}, nil
}

// NewRegisterCaller creates a new read-only instance of Register, bound to a specific deployed contract.
func NewRegisterCaller(address common.Address, caller bind.ContractCaller) (*RegisterCaller, error) {
	contract, err := bindRegister(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &RegisterCaller{contract: contract}, nil
}

// NewRegisterTransactor creates a new write-only instance of Register, bound to a specific deployed contract.
func NewRegisterTransactor(address common.Address, transactor bind.ContractTransactor) (*RegisterTransactor, error) {
	contract, err := bindRegister(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &RegisterTransactor{contract: contract}, nil
}

// NewRegisterFilterer creates a new log filterer instance of Register, bound to a specific deployed contract.
func NewRegisterFilterer(address common.Address, filterer bind.ContractFilterer) (*RegisterFilterer, error) {
	contract, err := bindRegister(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &RegisterFilterer{contract: contract}, nil
}

// bindRegister binds a generic wrapper to an already deployed contract.
func bindRegister(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(RegisterABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Register *RegisterRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Register.Contract.RegisterCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Register *RegisterRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Register.Contract.RegisterTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Register *RegisterRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Register.Contract.RegisterTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Register *RegisterCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Register.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Register *RegisterTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Register.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Register *RegisterTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Register.Contract.contract.Transact(opts, method, params...)
}

// GetAllTransactionsByClientID is a free data retrieval call binding the contract method 0x0691e1ee.
//
// Solidity: function getAllTransactionsByClientID(string id) view returns((string,string,string,uint256,uint256,string,bytes32)[] asSender, (string,string,string,uint256,uint256,string,bytes32)[] asReceiver)
func (_Register *RegisterCaller) GetAllTransactionsByClientID(opts *bind.CallOpts, id string) (struct {
	AsSender   []RegisterBankTransaction
	AsReceiver []RegisterBankTransaction
}, error) {
	var out []interface{}
	err := _Register.contract.Call(opts, &out, "getAllTransactionsByClientID", id)

	outstruct := new(struct {
		AsSender   []RegisterBankTransaction
		AsReceiver []RegisterBankTransaction
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.AsSender = *abi.ConvertType(out[0], new([]RegisterBankTransaction)).(*[]RegisterBankTransaction)
	outstruct.AsReceiver = *abi.ConvertType(out[1], new([]RegisterBankTransaction)).(*[]RegisterBankTransaction)

	return *outstruct, err

}

// GetAllTransactionsByClientID is a free data retrieval call binding the contract method 0x0691e1ee.
//
// Solidity: function getAllTransactionsByClientID(string id) view returns((string,string,string,uint256,uint256,string,bytes32)[] asSender, (string,string,string,uint256,uint256,string,bytes32)[] asReceiver)
func (_Register *RegisterSession) GetAllTransactionsByClientID(id string) (struct {
	AsSender   []RegisterBankTransaction
	AsReceiver []RegisterBankTransaction
}, error) {
	return _Register.Contract.GetAllTransactionsByClientID(&_Register.CallOpts, id)
}

// GetAllTransactionsByClientID is a free data retrieval call binding the contract method 0x0691e1ee.
//
// Solidity: function getAllTransactionsByClientID(string id) view returns((string,string,string,uint256,uint256,string,bytes32)[] asSender, (string,string,string,uint256,uint256,string,bytes32)[] asReceiver)
func (_Register *RegisterCallerSession) GetAllTransactionsByClientID(id string) (struct {
	AsSender   []RegisterBankTransaction
	AsReceiver []RegisterBankTransaction
}, error) {
	return _Register.Contract.GetAllTransactionsByClientID(&_Register.CallOpts, id)
}

// GetTransactionByID is a free data retrieval call binding the contract method 0x70a9db31.
//
// Solidity: function getTransactionByID(string id) view returns((string,string,string,uint256,uint256,string,bytes32))
func (_Register *RegisterCaller) GetTransactionByID(opts *bind.CallOpts, id string) (RegisterBankTransaction, error) {
	var out []interface{}
	err := _Register.contract.Call(opts, &out, "getTransactionByID", id)

	if err != nil {
		return *new(RegisterBankTransaction), err
	}

	out0 := *abi.ConvertType(out[0], new(RegisterBankTransaction)).(*RegisterBankTransaction)

	return out0, err

}

// GetTransactionByID is a free data retrieval call binding the contract method 0x70a9db31.
//
// Solidity: function getTransactionByID(string id) view returns((string,string,string,uint256,uint256,string,bytes32))
func (_Register *RegisterSession) GetTransactionByID(id string) (RegisterBankTransaction, error) {
	return _Register.Contract.GetTransactionByID(&_Register.CallOpts, id)
}

// GetTransactionByID is a free data retrieval call binding the contract method 0x70a9db31.
//
// Solidity: function getTransactionByID(string id) view returns((string,string,string,uint256,uint256,string,bytes32))
func (_Register *RegisterCallerSession) GetTransactionByID(id string) (RegisterBankTransaction, error) {
	return _Register.Contract.GetTransactionByID(&_Register.CallOpts, id)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Register *RegisterCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Register.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Register *RegisterSession) Owner() (common.Address, error) {
	return _Register.Contract.Owner(&_Register.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Register *RegisterCallerSession) Owner() (common.Address, error) {
	return _Register.Contract.Owner(&_Register.CallOpts)
}

// AddTransaction is a paid mutator transaction binding the contract method 0xf5f3fb00.
//
// Solidity: function addTransaction(string transactionID, string senderID, string receiverID, uint256 amount, uint256 date, string note) returns()
func (_Register *RegisterTransactor) AddTransaction(opts *bind.TransactOpts, transactionID string, senderID string, receiverID string, amount *big.Int, date *big.Int, note string) (*types.Transaction, error) {
	return _Register.contract.Transact(opts, "addTransaction", transactionID, senderID, receiverID, amount, date, note)
}

// AddTransaction is a paid mutator transaction binding the contract method 0xf5f3fb00.
//
// Solidity: function addTransaction(string transactionID, string senderID, string receiverID, uint256 amount, uint256 date, string note) returns()
func (_Register *RegisterSession) AddTransaction(transactionID string, senderID string, receiverID string, amount *big.Int, date *big.Int, note string) (*types.Transaction, error) {
	return _Register.Contract.AddTransaction(&_Register.TransactOpts, transactionID, senderID, receiverID, amount, date, note)
}

// AddTransaction is a paid mutator transaction binding the contract method 0xf5f3fb00.
//
// Solidity: function addTransaction(string transactionID, string senderID, string receiverID, uint256 amount, uint256 date, string note) returns()
func (_Register *RegisterTransactorSession) AddTransaction(transactionID string, senderID string, receiverID string, amount *big.Int, date *big.Int, note string) (*types.Transaction, error) {
	return _Register.Contract.AddTransaction(&_Register.TransactOpts, transactionID, senderID, receiverID, amount, date, note)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Register *RegisterTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Register.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Register *RegisterSession) RenounceOwnership() (*types.Transaction, error) {
	return _Register.Contract.RenounceOwnership(&_Register.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Register *RegisterTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Register.Contract.RenounceOwnership(&_Register.TransactOpts)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Register *RegisterTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Register.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Register *RegisterSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Register.Contract.TransferOwnership(&_Register.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Register *RegisterTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Register.Contract.TransferOwnership(&_Register.TransactOpts, newOwner)
}

// RegisterOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Register contract.
type RegisterOwnershipTransferredIterator struct {
	Event *RegisterOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *RegisterOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(RegisterOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(RegisterOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *RegisterOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *RegisterOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// RegisterOwnershipTransferred represents a OwnershipTransferred event raised by the Register contract.
type RegisterOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Register *RegisterFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*RegisterOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Register.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &RegisterOwnershipTransferredIterator{contract: _Register.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Register *RegisterFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *RegisterOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Register.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(RegisterOwnershipTransferred)
				if err := _Register.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Register *RegisterFilterer) ParseOwnershipTransferred(log types.Log) (*RegisterOwnershipTransferred, error) {
	event := new(RegisterOwnershipTransferred)
	if err := _Register.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
