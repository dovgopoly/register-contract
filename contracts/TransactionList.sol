// SPDX-License-Identifier: MIT

pragma solidity ^0.8.13;

import "./ComparableString.sol";
import "./Register.sol";

library TransactionList {
    using ComparableString for string;

    function countWithSenderID(
        Register.BankTransaction[] storage self,
        string memory id
    )
        private
        view
        returns(uint)
    {
        uint counter = 0;
        for (uint i = 0; i < self.length; i++) {
            if (self[i].senderID.isEqual(id)) {
                counter++;
            }
        }
        return counter;
    }

    function countWithReceiverID(
        Register.BankTransaction[] storage self,
        string memory id
    )
        private
        view
        returns(uint)
    {
        uint counter = 0;
        for (uint i = 0; i < self.length; i++) {
            if (self[i].receiverID.isEqual(id)) {
                counter++;
            }
        }
        return counter;
    }

    function filterSenderID(
        Register.BankTransaction[] storage self,
        string memory id
    )
        internal
        view
        returns(Register.BankTransaction[] memory filtered)
    {
        uint length = countWithSenderID(self, id);
        filtered = new Register.BankTransaction[](length);

        for (uint i = 0; i < self.length; i++) {
            if (self[i].senderID.isEqual(id)) {
                filtered[length - 1] = self[i];
                length--;
            }
        }
    }

    function filterReceiverID(
        Register.BankTransaction[] storage self,
        string memory id
    )
        internal
        view
        returns(Register.BankTransaction[] memory filtered)
    {
        uint length = countWithReceiverID(self, id);
        filtered = new Register.BankTransaction[](length);

        for (uint i = 0; i < self.length; i++) {
            if (self[i].receiverID.isEqual(id)) {
                filtered[length - 1] = self[i];
                length--;
            }
        }
    }
}
