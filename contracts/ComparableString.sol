// SPDX-License-Identifier: MIT

pragma solidity ^0.8.13;

library ComparableString {
    function isEqual(
        string memory self,
        string memory other
    )
        internal
        pure
        returns(bool)
    {
        return keccak256(bytes(self)) == keccak256(bytes(other));
    }
}
