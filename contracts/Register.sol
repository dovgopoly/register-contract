// SPDX-License-Identifier: MIT

pragma solidity ^0.8.13;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./TransactionList.sol";
import "./ComparableString.sol";

contract Register is Ownable {
    struct BankTransaction {
        string transactionID;
        string senderID;
        string receiverID;
        uint amount;
        uint date;
        string note;
        bytes32 hash;
    }

    using TransactionList for BankTransaction[];
    using ComparableString for string;

    BankTransaction[] transactions;
    mapping(string => bool) usedTxs;

    function _countHash(
        string memory transactionID,
        string memory clientID,
        string memory receiverID,
        uint amount,
        uint date
    )
        internal
        pure
        returns(bytes32)
    {
        return keccak256(
            abi.encodePacked(
                transactionID,
                clientID,
                receiverID,
                Strings.toString(amount),
                Strings.toString(date)
            )
        );
    }

    function addTransaction(
        string memory transactionID,
        string memory senderID,
        string memory receiverID,
        uint amount,
        uint date,
        string memory note
    )
        external
        onlyOwner
    {
        require(
            !usedTxs[transactionID],
            "Transaction with such id already exists"
        );
        require(
            !senderID.isEqual(receiverID),
            "Sender's id should not be equal receiver's one"
        );
        usedTxs[transactionID] = true;
        transactions.push(
            BankTransaction(
                transactionID,
                senderID,
                receiverID,
                amount,
                date,
                note,
                _countHash(transactionID, senderID, receiverID, amount, date)
            )
        );
    }

    function getTransactionByID(
        string memory id
    )
        external
        view
        returns(BankTransaction memory)
    {
        require(
            usedTxs[id],
            "Transaction with such id has not been found"
        );
        for (uint i = 0; i < transactions.length; i++) {
            if (transactions[i].transactionID.isEqual(id)) {
                return transactions[i];
            }
        }
        revert("Internal error");
    }

    function getAllTransactionsByClientID(
        string memory id
    )
        external
        view
        returns(
            BankTransaction[] memory asSender,
            BankTransaction[] memory asReceiver
        )
    {
        asSender = transactions.filterSenderID(id);
        asReceiver = transactions.filterReceiverID(id);
    }
}
